/*
 * 
 * 
 */
package bomba;

/**
 *
 * @author bbab9
 */
public class Bomba {
    private int idBomba;
    private float contador;
    private int capacidad;
    private Gasolina gasolina;
    private float totalVenta;
    
    // Constructores

    public Bomba(int idBomba, int contador, int capacidad, Gasolina gasolina) {
        this.idBomba = idBomba;
        this.contador = contador;
        this.capacidad = capacidad;
        this.gasolina = gasolina;
    }
    
    public Bomba(){
        this.idBomba = 0;
        this.contador = 0.0f;
        this.capacidad = 0;
        this.gasolina = new Gasolina();
        
    }

    public int getIdBomba() {
        return idBomba;
    }

    public void setIdBomba(int idBomba) {
        this.idBomba = idBomba;
    }

    public float getContador() {
        return contador;
    }

    public void setContador(float contador) {
        this.contador = contador;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    public float obtenerInventario(){
        return this.capacidad - this.contador;
    }
    
    public float venderGasolina(float cantidad){
        
        float litrosVendidos = 0.0f;
        
        if(cantidad>=this.obtenerInventario()){
            
            litrosVendidos = this.contador + cantidad;
            
        }
        return litrosVendidos;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
    
    
}
