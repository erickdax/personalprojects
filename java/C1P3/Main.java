/*
Clase Main para obtener y mostrar los datos de la practica 3
30 de mayo de 2024
Vazquez Lizarraga Erick
 */
package C1P3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        Persona persona1 = new Persona();
        Persona.Domicilio domicilio = persona1.new Domicilio();
    
        int opc;
        do{ //Ciclo para que se repita el menu hasta que se cierre  
            System.out.println("---MENÚ DE OPCIONES---");
            System.out.println("1.- Insertar datos");
            System.out.println("2.- Imprimir datos");
            System.out.println("3.- Cerrar");
            
            opc = leer.nextInt(); leer.nextLine();
            
            switch(opc){//Switch para escoger la opcion del menu
                case 1:
                     // Solicitar al usuario los datos de la persona
                     System.out.println("Inserta el nombre de la persona:");
                     persona1.setNombre(leer.nextLine());

                     // Solicitar al usuario los datos del domicilio
                     System.out.println("\nInserta la calle del domicilio:");
                     domicilio.setCalle(leer.nextLine());
        
                     System.out.println("Inserta el número del domicilio:");
                     domicilio.setNumero(leer.nextInt()); leer.nextLine();
        
                     System.out.println("Inserta la colonia del domicilio:");
                     domicilio.setColonia(leer.nextLine());
                     persona1.setDomicilio(domicilio);
                     break;
                case 2:
                    persona1.imprimirDatosPersona();
                    domicilio.imprimirDomicilio();
                    break;
            }
        } while(opc !=3);
    }
}
