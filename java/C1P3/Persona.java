/*
Practica 3 clases internas. CLASE PADRE PERSONAS
30 DE MAYO DE 2024
Vazquez Lizarraga Erick
 */
package C1P3;

public class Persona {
    //Atributos
    protected String nombre;
    protected Domicilio domicilio;
    
    //constructores vacio, parametros y de copia
    public Persona(){
        this.domicilio = domicilio;
        this.nombre = " ";
    
    }
    public Persona(String nombre, Domicilio domicilio){
        
    }
    public Persona(Persona persona){
        this.domicilio = persona.domicilio;
        this.nombre = persona.nombre;
    }
    
    //encapsulaiento de todos los atributos
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }
    
    
    //metodos de imprimir datos personal
    public void imprimirDatosPersona(){
        System.out.println("El nombre de la persona es: "+ this.nombre);
}
    
    //clase interna domicilio
    public class Domicilio{
     //atributos de clase interna domicilio
     protected String calle;
     protected int numero;
     protected String colonia;
     
     //constructores de vacio, parametros y copia
     public Domicilio(){
         this.calle = " ";
         this.colonia = " ";
         this.numero = 0;
     }
     public Domicilio(String calle, int numero, String colonia){
         this.calle = calle;
         this.colonia = colonia;
         this.numero = numero;
     }
     //encapsulmiento de los atributos de la clase Domicilio

        public String getCalle() {
            return calle;
        }

        public void setCalle(String calle) {
            this.calle = calle;
        }

        public int getNumero() {
            return numero;
        }

        public void setNumero(int numero) {
            this.numero = numero;
        }

        public String getColonia() {
            return colonia;
        }

        public void setColonia(String colonia) {
            this.colonia = colonia;
        }
     
    //metodo para imprimir domicilio
     public void imprimirDomicilio(){
         System.out.println("La colonia de la persona es: "+ this.colonia);
         System.out.println("La calle de la persona es: "+this.calle);
         System.out.println("El numero de la persona es: "+ this.numero);
    }
  }
}
