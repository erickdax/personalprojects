/*
 * Erick Vazquez Lizarraga
 * Interfaz de la practica 2 de interfaces
 * 20/05/2024
 */
package C1P2;

public interface Finanzas {
    //Atributos
    public double TASA = 0.15;
    //Metodos
    public void sumarInteres();
}
