/*
 * Erick Vazquez Lizarraga
 * Clase hija de la practica 2 de interfaces
 * 20/05/2024
 */
package C1P2;

public class CuentaFinancieraFinal extends CuentaFinanciera implements Finanzas{
    //Atributos
    private double saldo;
    
    //Constructores
    //Constructor vacio
    public CuentaFinancieraFinal(){
        this.saldo = 0;
    }
    
    //Constructor de parametros
    public CuentaFinancieraFinal(double saldo){
        this.saldo = saldo;
    }
    
    //Constructor de copia
    public CuentaFinancieraFinal(CuentaFinancieraFinal cuentaFinancieraFinal){
        this.saldo = cuentaFinancieraFinal.saldo;
    }
    
    //Encapsulamiento

    public double getSaldo() {
        return saldo;
    }
    
    //Metodos
    public void depositar (double saldo){
        this.saldo += saldo;
    }
    
    public void sumarInteres(){
        this.saldo += (getSaldo() * TASA);
    }

    public void imprimir(){
        super.imprimir();
        System.out.println("Saldo: " + this.getSaldo());
        System.out.println("=====================\n");
    }
}
