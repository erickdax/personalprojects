/*
 * Erick Vazquez Lizarraga
 * Clase main de la practica 2 de interfaces
 * 20/05/2024
 */
package C1P2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Variable para la lectura de datos
        Scanner leer = new Scanner(System.in);
        
        //Variable para menu
        int op = 0;
        //Creacion del objeto CuentaFinancieraFinal
        CuentaFinancieraFinal cuentaFinancieraFinal = new CuentaFinancieraFinal();
        
            System.out.println("\nIngresa el nombre del titular: ");
            cuentaFinancieraFinal.setTitular(leer.nextLine());
            System.out.println("================================\n");
            
        //Creacion del Menu
        do {          
            
            
            System.out.println("Nombre del titular: " + cuentaFinancieraFinal.getTitular());
            System.out.println("Saldo inicial: " + cuentaFinancieraFinal.getSaldo());
            
            
            
            System.out.println("\nMENU DE OPCIONES");
            System.out.println("\n1. Depositar");
            System.out.println("\n2. Sumar intereses");
            System.out.println("\n3. Imprimir");
            System.out.println("\n4. Cerrar");
            op = leer.nextInt();
            
            switch (op) {
                case 1:
                    System.out.println("\nOpcion seleccionada: " + op);
                    System.out.print("\nCantidad a depositar: ");
                    double x = leer.nextDouble(); leer.nextLine();
                    cuentaFinancieraFinal.depositar(x);
                    break;
                    
                case 2:
                    System.out.println("\nOpcion seleccionada: " + op);
                    cuentaFinancieraFinal.sumarInteres();
                    break;
                    
                case 3:
                    System.out.println("\nOpcion seleccionada: " + op);
                    cuentaFinancieraFinal.imprimir();
                    break;
                    
                default:
                    System.out.println("Adios...");
            }
        } while (op != 4);//Condicion para el ciclo do-while
    }
    
}
