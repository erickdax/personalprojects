/*
 * Erick Vazquez Lizarraga
 * Clase padre de la practica 2 de interfaces
 * 20/05/2024
 */
package C1P2;



public abstract class CuentaFinanciera {
    //Atributos
    protected String titular;
    
    //Constructores
    //Constructor vacio
    public CuentaFinanciera(){
        this.titular = "";
    }
    //Constructor de parametros
    public CuentaFinanciera(String titular){
        this.titular = titular;
    }
    
    //Constructor de copia
    public CuentaFinanciera(CuentaFinanciera cuentaFinanciera){
        this.titular = cuentaFinanciera.titular;
    }
    
    //Encapsulamiento
    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }
    
    public abstract double getSaldo();
    
    //Metodos
    public void imprimir(){
        System.out.println("\nDATOS DE LA CUENTA");
        System.out.println("\nNombre del titular: " + this.getTitular());
    }
}
