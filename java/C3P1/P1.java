/*
 * Nombre: Erick Vázquez Lizárraga
 * Fecha 11/07/2024
 */
package C3P1;

import java.util.Scanner;

/**
 *
 * @author erick
 */
public class P1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int op = 0;
        int numAlumnos = 1;
        String[]names = new String[100];
        float[] cals = new float[100];
            
        do {            
            Scanner scan = new Scanner(System.in);
            System.out.println("Qué quieres hacer?");
            System.out.println(" 1- Agregar un alumno y su calificación");
            System.out.println(" 2- Modificar una calificación");
            System.out.println(" 3- Mostrar calificaciones");
            System.out.println(" 4- Salir");
            op = scan.nextInt(); scan.nextLine();
            
            
            switch (op) {
                case 1:
                    System.out.println("Cuál es el nombre del alumno?");
                    names[numAlumnos - 1] = scan.nextLine();
                    System.out.println("Cuál es la calificación del alumno?");
                    cals[numAlumnos - 1] = scan.nextFloat();
                    System.out.println("Las calificaciones son: ");
                    numAlumnos = numAlumnos + 1;
                    
                    
                    
                    break;
                case 2:
                    System.out.println("La calificación de quién quieres modificar?");
                    String search = scan.nextLine();
                    
                    for (int i = 0; i < numAlumnos; i++) {
                        if (names[i] == search) {
                            System.out.println("La calificación de: " + names[i] + " es: " + cals[i]);
                            System.out.println("Cuál será la nueva calificación?");
                            cals[i] = scan.nextFloat();
                            System.out.println("Muy bien...");
                            System.out.println("La calificación de: " + names[i] + " ahora es: " + cals[i]);
                        }
                    }
                    
                    break;
                
                case 3:
                    // Primero ordenamos las calificaciones por menor y mayor
                    float calMayor = 0;
                    for (int i = 0; i < (numAlumnos -1); i++) {
                        
                        
                        if (cals[i] > calMayor) {
                            
                        }
                        
                    }
                    
                    // Aquí imprimimos las calificaciones ya impresas
                    System.out.println("Las calificaciones son: ");
                    for (int i = 0; i < (numAlumnos -1); i++) {
                        System.out.println("#" + (i+1) + " Nombre: " + names[i] + ", Calificación: " + cals[i]);
                        
                    }
                    
                    
                    break;
                    
                case 4:
                    
                    System.out.println("Saliendo...");
                    
                    break;
                default:
                    throw new AssertionError();
            }
            
            
            
        } while (op != 4);
        
    }
    
}
