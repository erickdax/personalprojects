/*
 * 
 * 
 */
package EXC1;

import java.util.Scanner;

/**
 *
 * @author bbab9
 */
public class Venta extends Transaccion{
    // Atributos
    private Cliente cliente;
    
    // Constructores
    public Venta(){
        this.cliente = new Cliente();
    }

    public Venta(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public Venta(Venta t){
        this.cliente = t.cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    
    @Override
    public void capturar(){
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Ingresa el ID de transaccion: ");
        super.id = scan.nextInt();
        System.out.println("Ingresa el monto de la venta: ");
        super.monto = scan.nextFloat();
        System.out.println("Ingresa la fecha de la venta: ");
        super.fecha = scan.nextLine(); 
        super.setFecha(fecha); scan.nextLine();
        System.out.println("Escribe el nombre del cliente");
        this.cliente.setNombre(scan.nextLine());
        System.out.println("Escribe el RFC del cliente:");
        this.cliente.setRfc(scan.nextLine());
        
        
    }

    @Override
    public void imprimir(){
        super.imprimir();
        System.out.println("Estos son los datos del cliente: \n"
                    + "Nombre: " + cliente.nombre
                    + "\n RFC: " + cliente.rfc);
    }
    
    
    
    
    
    public class Cliente{ // Definicion de la clase interna Cliente
        // Atributos
        private String nombre;
        private String rfc;
        
        // Constructores
        
        public Cliente(){
            this.nombre ="";
            this.rfc = "";
        }

        public Cliente(String nombre, String rfc) {
            this.nombre = nombre;
            this.rfc = rfc;
        }
        
        public Cliente(Cliente t){
            this.nombre = t.nombre;
            this.rfc = t.rfc;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getRfc() {
            return rfc;
        }

        public void setRfc(String rfc) {
            this.rfc = rfc;
        }
        
        public String imprimeCliente(){
            return "Estos son los datos del cliente: \n"
                    + this.nombre
                    + " " + this.rfc;
        }
    }
}
