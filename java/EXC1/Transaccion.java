/*
 * 
 * 
 */
package EXC1;

/**
 *
 * @author bbab9
 */
public abstract class Transaccion implements Recibos {
    protected int id;
    protected float monto;
    protected String fecha;

    public Transaccion() {
    }

    
    
    public Transaccion(int id, float monto, String fecha) {
        this.id = id;
        this.monto = monto;
        this.fecha = fecha;
    }
    public Transaccion (Transaccion t){
        this.id = t.id;
        this.monto = t.id;
        this.fecha = t.fecha;
    }    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    @Override
    public void imprimir(){
        System.out.println("ID: " + this.getId());
        System.out.println("Monto: " + this.getMonto());
        System.out.println("Fecha: " + this.fecha);
    }
    
    public abstract void capturar();
}
