/*
 * 
 * 
 */
package EXC1;

import java.util.Scanner;

/**
 *
 * @author bbab9
 */
public class PruebaTransacciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Venta venta = new Venta();
        Compra compra = new Compra();
                int op = 0;
                
        
        do {  
        
            System.out.println("Selecciona la opcion que deseas realizar.");
            System.out.println("1- Venta");
            System.out.println("2- Compra");
            System.out.println("3- Salir");
            op = scan.nextInt();
        
            
            switch (op) {
                case 1:
                    venta.capturar();
                    System.out.println("==================");
                    venta.imprimir();
                    break;
                  
                case 2:
                    compra.capturar();
                    compra.imprimir();
                    break;
                
            }
            
        } while (op != 3);
        
        
    }
    
}
