/*
 * 
 * 
 */
package EXC1;

import java.util.Scanner;

/**
 *
 * @author bbab9
 */
public class Compra extends Transaccion {
    
    // Atributos
    private Proveedor proveedor;
    
    // Constructores

    public Compra() {
        this.proveedor = new Proveedor();
    }

    public Compra(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    
    
    public void imprimir(){
        super.imprimir();
        System.out.println("Estos son los datos del proveedor: \n"
                    + "Nombre: " + proveedor.nombre
                    + "\n RFC: " + proveedor.telefono);
    }
    
    public void capturar(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingresa el ID de transaccion: ");
        super.id = scan.nextInt();
        System.out.println("Ingresa el monto de la compra: ");
        super.monto = scan.nextFloat();
        System.out.println("Ingresa la fecha de la compra: ");
        super.fecha = scan.nextLine(); scan.nextLine();
        System.out.println("Escribe el nombre del proveedor");
        this.proveedor.setNombre(scan.nextLine());
        System.out.println("Escribe el numero de telefono del proveedor:");
        this.proveedor.setTelefono(scan.nextLine());
        
        
    }
        
   
    
            
            
            
    public class Proveedor{ // Se define la clase interna Proveedor
        
        // Atributos
        private String nombre;
        private String telefono;
        
        // Constructor vacío
        public Proveedor(){
            this.nombre = "";
            this.telefono = "";
        }
        
        // Constructor de parámetros
        public Proveedor(String nombre, String telefono) {
            this.nombre = nombre;
            this.telefono = telefono;
        }
        
        // Constructor de copia
        public Proveedor(Proveedor t){
            this.nombre = t.nombre;
            this.telefono = t.telefono;
        }
        
        // Encapsulamiento
        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getTelefono() {
            return telefono;
        }

        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }
        
        // Métodos
        public String imprimeProveedor(){
            
            
            return "Estos son los datos del proveedor: \n"
                    + getNombre()
                    + " " + getTelefono();
        }
    }
}
