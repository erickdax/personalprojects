/*
 * Nombre: Erick Vázquez Lizárraga
 * Fecha: 10/07/2024
 * Descripción: Ejemplo de excepciones
 */
package C3TrabajosClase;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbab9
 */
public class Divisiones {
    private float dividendo;
    private float divisor;
    private float cociente;
    
    public Divisiones(){
        this.dividendo = 0.0f;
        this.divisor = 0.0f;
        this.cociente = 0.0f;
    }

    public Divisiones(float dividendo, float divisor, float cociente) {
        this.dividendo = dividendo;
        this.divisor = divisor;
        this.cociente = cociente;
    }
    
    public Divisiones(Divisiones d){
        this.dividendo = d.dividendo;
        this.divisor = d.divisor;
        this.cociente = d.cociente;
    }

    public float getDividendo() {
        return dividendo;
    }

    public void setDividendo(float dividendo) {
        this.dividendo = dividendo;
    }

    public float getDivisor() {
        return divisor;
    }

    public void setDivisor(float divisor) {
        this.divisor = divisor;
    }

    public float getCociente() {
        return cociente;
    }

    public void setCociente(float cociente) {
        this.cociente = cociente;
    }
    
    public void dividir(){
        try{
        Scanner scan = new Scanner(System.in);
        System.out.println("Dividendo: ");
        this.setDividendo(scan.nextFloat()); scan.nextLine();
        System.out.println("Divisor: ");
        this.setDivisor(scan.nextFloat()); scan.nextLine();
        divide();
        }catch(InputMismatchException e){ // Catch utilizado cuando se introduce una letra
            System.out.println("Datos incorrectos");
        } catch (ArithmeticException e) {
            System.out.println("" + e.getMessage() );
        }catch (Exception e) {
            System.out.println("" + e.getMessage() );
        }finally{
            System.out.println("Cociente: " + this.getCociente());
        }
    }
    
    private void divide() throws ArithmeticException{
        
        if(this.divisor == 0){
            throw new ArithmeticException("No es posible la división entre cero");
            
        }else{
            this.cociente = this.dividendo/this.divisor;
        }
        
    }
}
