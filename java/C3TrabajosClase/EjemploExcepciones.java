/*
 * Nombre: Erick Vázquez Lizárraga
 * Fecha: 10/07/2024
 * Descripción: Ejemplo de excepciones
 */
package C3TrabajosClase;

/**
 *
 * @author bbab9
 */
public class EjemploExcepciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Divisiones d = new Divisiones();
        
        d.dividir();
    }
    
}
