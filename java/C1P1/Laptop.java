/*
 * Erick Vazquez Lizarraga
 * Clase hija de la practica 1 de abstraccion
 * 13/05/2024
 */
package C1P1;

import java.util.Scanner;

public class Laptop extends Dispositivos{
    Scanner leer = new Scanner(System.in); //Variable para la lectura de datos
    
    //Atributos
    private String sisOp, numSerie;
    
    //Constructores
    //Constructor vacio
    public Laptop(){
        this.sisOp = "";
        this.numSerie = "";
    }
    
    //Constructor de parametros
    public Laptop(String modelo, String compañia, String numero){
        this.sisOp = modelo;
        this.numSerie = compañia;
    }
    
    //Constructor de copia
    public Laptop(Laptop laptop){
        this.sisOp = laptop.sisOp;
        this.numSerie = laptop.numSerie;
    }

    //Encapsulamiento
    public String getSisOp() {
        return sisOp;
    }

    public void setSisOp(String sisOp) {
        this.sisOp = sisOp;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }
   
    
    //Metodos
    //Sobrescritura
    @Override
    public void Estado(){
        //Pedir el estado del Celular
        System.out.println("Ingresa el estado del dispositivo por favor.\n - Encendido [TRUE]\n - Apagado [FALSE]");
        this.setEstado(leer.nextBoolean()); leer.nextLine();
        
        //Condicion para validar si está encedido o apagado
        if (this.getEstado() == true) {
            System.out.println("\nIngresa el sistema operativo de la laptop: ");
            this.setSisOp(leer.nextLine());
            System.out.println("\nIngresa el numero de serie de la laptop: ");
            this.setNumSerie(leer.nextLine());  
        } 
        else {
            System.out.println("\nEl dispositivo está apagado...");
        }
        
        //Impresion de datos
        if (this.getEstado() == true) {
        System.out.println("El dispositivo está encendido");
        System.out.println("El sistema operativo de la laptop es: " + this.getSisOp());
        System.out.println("El numero de serie de la laptop es: " + this.getNumSerie());
        }
        
    }
    
    
}
