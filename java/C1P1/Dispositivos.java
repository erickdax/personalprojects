/*
 * Erick Vazquez Lizarraga
 * Clase padre de la practica 1 de abstraccion
 * 13/05/2024
 */
package C1P1;

import java.util.Scanner;

public abstract class Dispositivos {
    
    //Atributos
    protected boolean estado;
    
    //Constructores
    //Constructor vacio
    public Dispositivos(){
        this.estado = false;
    }
    
    //Constructor de parametros
    public Dispositivos(boolean estado){
        this.estado = estado;
    }
    
    //Constructor de copia
    public Dispositivos(Dispositivos dispositivos){
        this.estado = dispositivos.estado;
    }
    
    //Encapsulamiento

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    
    //Metodos
    public abstract void Estado();
    
}
