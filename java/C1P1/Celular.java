/*
 * Erick Vazquez Lizarraga
 * Clase hija de la practica 1 de abstraccion
 * 13/05/2024
 */
package C1P1;

import java.util.Scanner;

public class Celular extends Dispositivos{
    Scanner leer = new Scanner(System.in); //Variable para la lectura de datos
    
    //Atributos
    private String modelo, compañia, numero;
    
    //Constructores
    //Constructor vacio
    public Celular(){
        this.modelo = "";
        this.compañia = "";
        this.numero = "";
    }
    
    //Constructor de parametros
    public Celular(String modelo, String compañia, String numero){
        this.modelo = modelo;
        this.compañia = compañia;
        this.numero = numero;
    }
    
    //Constructor de copia
    public Celular(Celular celular){
        this.modelo = celular.modelo;
        this.compañia = celular.compañia;
        this.numero = celular.numero;
    }
    public String getModelo() {    
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }

    public String getNumero() {
        return numero;
    }

    //Encapsulamiento
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    //Metodos
    //Sobrescritura
    @Override
    public void Estado(){
        //Pedir el estado del Celular
        System.out.println("Ingresa el estado del dispositivo por favor.\n - Encendido [TRUE]\n - Apagado [FALSE]");
        this.setEstado(leer.nextBoolean()); leer.nextLine();
        
        //Condicion para validar si está encedido o apagado
        if (this.getEstado() == true) {
            System.out.println("\nIngresa el modelo del celular: ");
            this.setModelo(leer.nextLine());
            System.out.println("\nIngresa la compañia del celular: ");
            this.setCompañia(leer.nextLine());
            System.out.println("\nIngresa el numero del celular: ");
            this.setNumero(leer.nextLine());
        } 
        else {
            System.out.println("\nEl dispositivo está apagado...");
        }
        
        //Impresion de datos
        if (this.getEstado() == true) {
        System.out.println("El dispositivo está encendido");
        System.out.println("El modelo del celular es: " + this.getModelo());
        System.out.println("La compañia del celular es: " + this.getCompañia());
        System.out.println("El numero del celular es: " + this.getNumero());    
        }
        
    }
    
    
}
