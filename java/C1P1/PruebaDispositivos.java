/*
 * Erick Vazquez Lizarraga
 * Clase main de la practica 1 de abstraccion
 * 13/05/2024
 */
package C1P1;

import java.util.Scanner;

public class PruebaDispositivos {
    
    public static void main(String[] args) {
// Scanner read para leer datos
        Scanner read = new Scanner(System.in);

        // Pedirle datos al usuario
        System.out.print("Cuantos dispositivos desea capturar: ");
        int d = read.nextInt(); read.nextLine();

        // Arreglo de tipo dipositivos
        Dispositivos[] arreglo = new Dispositivos[d];

        int i = 0;
        do {
            System.out.println("Menú de opciones");
            System.out.println("1 - Celular");
            System.out.println("2 - Laptop");
            System.out.print("Seleccione una opción: ");
            int op = read.nextInt(); read.nextLine();

            switch (op){
                case 1: arreglo[i] = new Celular(); // polimorfismo
                    break;
                case 2: arreglo[i] = new Laptop(); // polimorfismo
                    break;
            }

            arreglo[i].Estado();
            i++;
        }while(i < d);    }
    
}
